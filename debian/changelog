f2j (0.8.1+dfsg-5) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Fix build issue with gcc-10
    Closes: #957190

 -- Andreas Tille <tille@debian.org>  Wed, 12 Aug 2020 12:02:31 +0200

f2j (0.8.1+dfsg-4) unstable; urgency=medium

  * debhelper 12
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0
  * Drop useless get-orig-source target
  * Remove trailing whitespace in debian/changelog

 -- Andreas Tille <tille@debian.org>  Sat, 12 Jan 2019 07:44:30 +0100

f2j (0.8.1+dfsg-3) unstable; urgency=medium

  * Team upload.

  [ Chris West (Faux) ]
  * (Closes:#873979): fix build on java 9 by updating
    replace_printfformat.patch and choosing VER_TARGET=1.8.

  [ Markus Koschany ]
  * Switch to compat level 10.
  * Declare compliance with Debian Policy 4.1.1.
  * Use https for Format field.

 -- Markus Koschany <apo@debian.org>  Thu, 26 Oct 2017 01:02:45 +0200

f2j (0.8.1+dfsg-2) unstable; urgency=medium

  * cme fix dpkg-control
  * upload to unstable (main)
  * better hardening

 -- Andreas Tille <tille@debian.org>  Fri, 24 Jun 2016 13:41:01 +0200

f2j (0.8.1+dfsg-1) experimental; urgency=medium

  * Remove non-free parts from the code and upload to main
  * Moved packaging from Debian Med SVN to Debian Java Git
  * debhelper 9
  * cme fix dpkg-control
  * DEP5 fixes
  * Fix some spelling issues
  * Propagate hardening options

 -- Andreas Tille <tille@debian.org>  Fri, 29 Jan 2016 15:47:50 +0100

f2j (0.8.1-2) unstable; urgency=low

  * debian/copyright: Add missing statement on non commercial usage

 -- Olivier Sallou <osallou@debian.org>  Thu, 24 May 2012 14:25:02 +0200

f2j (0.8.1-1) unstable; urgency=low

  [ Olivier Sallou ]
  * Initial release (Closes: #657184)
  * Move to non-free, license modification to DFSG compliant license
    in progress

  [ Thorsten Alteholz ]
  * debian/rules: target get-orig-source added

 -- Olivier Sallou <osallou@debian.org>  Tue, 24 Jan 2012 17:34:50 +0100
